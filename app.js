const express = require("express");
const path = require("path");
const axios = require("axios");
const cors = require("cors");

const app = express();

app.use(cors({credentials: true, origin: true}))

app.get("/", (req, res) => {
  res.end("Hello Express");
});


app.get("/getBNBprice", (req, res) => {
  const page = req.query.page || 1;
  const pageSize = req.query.pageSize || 10;
  axios.get("https://fapi.binance.com/fapi/v1/ticker/24hr?symbol=BNBUSDT").then(({ data }) => {
    res.setHeader("Content-Type", "application/json")
    res.end(JSON.stringify(data))
  });
});

app.listen(8888, () => {
  console.log("server is listening in http://localhost:8888")
})